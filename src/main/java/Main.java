import org.example.dao.AddressDao;
import org.example.dao.PersonDao;
import org.example.entities.Address;
import org.example.entities.AddressID;
import org.example.entities.BuildingType;
import org.example.entities.Person;

import java.util.HashMap;
import java.util.Map;

public class Main {

    private static AddressDao addressDao = new AddressDao();
    private static PersonDao personDao = new PersonDao();

    public static void main(String[] args) {

        AddressID addressId = new AddressID("Primaverii", 14, "Cluj-Napoca");
        Address address = new Address(addressId, 150, BuildingType.PRIVATE_HOUSE);
        addressDao.createAddress(address);

        AddressID addressId2 = new AddressID("Dunari", 14, "Bucuresti");
        Address address1 = new Address(addressId2, 180, BuildingType.OFFICE);
        addressDao.createAddress(address1);

        AddressID addressId3 = new AddressID("Mures", 14, "Olt");
        Address address2 = new Address(addressId3, 200, BuildingType.DOUPLEX);
        addressDao.createAddress(address2);

//        AddressDao addressDao = new AddressDao();

        Person firstPerson = new Person("1980101303930", "Moromete", 1998, address);
        personDao.createPerson(firstPerson);

        // am creat un obiect in care salvam ce ne da readAddress
        Address a1 = addressDao.readAddress(addressId);
        System.out.println(a1);
        System.out.println(personDao.readPerson("1980101303930"));

        Person ion = new Person("193040567842345", "Ion", 1993, address1);
        Person vlad = new Person("177040567842345", "Vlad", 1977, address);
        Person mihai = new Person("186040567842345", "Mihai", 1986, address2);
        Person dorin = new Person("168040567842345", "Dorin", 1968, address);
        Person maria = new Person("289121298983556", "Maria", 1989, address);
        Person ioana = new Person("299121298983556", "Ioana", 1999, address);
        Person simona = new Person("278121298983556", "Simona", 1978, address);

        personDao.createPerson(ion);
        personDao.createPerson(vlad);
        personDao.createPerson(mihai);
        personDao.createPerson(dorin);
        personDao.createPerson(maria);
        personDao.createPerson(ioana);
        personDao.createPerson(simona);


        // ce este in stanga de sagata, sunt parametrii din metoda
        // ce este in dreapta de sageata, este corpul metodei
        // v1
        personDao.readAllPerson().forEach((p) -> {
            prettyDisplayOfPerson(p);
        });
        // v1
        personDao.readAllPerson().forEach((p) -> {
            prettyDisplayOfPerson(p);
        });

        // v2
        // personDao.readAllPerson().forEach(p -> prettyDisplayOfPerson(p));

        // v3
        // personDao.readAllPerson().forEach(Main::prettyDisplayOfPerson);
        Map<Integer, String> numbers = new HashMap<>();
        numbers.put(11, "unsprezece");
        numbers.put(100, "o sută");
        numbers.put(-2, "minus doi");

        numbers.forEach((k, v) -> {
            System.out.println("Numărul " + k + " se pronunță " + v);
        });
        numbers.forEach((k, v) -> prettyPrint(k, v));

        numbers.forEach(Main::prettyPrint);

        simona.setName("Irina");
        personDao.updatePerson(simona);

        personDao.removePerson(maria);

        System.out.println("-------------");

        personDao.readAllPerson().forEach(p -> System.out.println(p));

        System.out.println("Young");
        personDao.youngThan(1990).forEach(System.out::println);

        System.out.println("-------------");

        personDao.allFrom("Cluj-Napoca").forEach(System.out::println);
    }

    static void prettyDisplayOfPerson(Person person) {
        System.out.println(person.getName() +
                " nascut in " + person.getYearOfBirthday() +
                " locuieste in " + person.getAddress().getId().getCity());
    }

    static void prettyPrint(Integer nr, String traducere) {
        System.out.println("+++ Numarul " + nr + " se pronunta " + traducere + " ++++");
    }

}
