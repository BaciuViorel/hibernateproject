package org.example.entities;

import jakarta.persistence.Embeddable;

@Embeddable // pentru ca este cheie primara compusa pentru Address
public class AddressID {
    private String streetName;
    private Integer streetNr;
    private String city;

    public AddressID() {
    }

    public AddressID(String streetName, Integer streetNr, String city) {
        this.streetName = streetName;
        this.streetNr = streetNr;
        this.city = city;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public Integer getStreetNr() {
        return streetNr;
    }

    public void setStreetNr(Integer streetNr) {
        this.streetNr = streetNr;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "AddressID{" +
                "streetName='" + streetName + '\'' +
                ", streetNr=" + streetNr +
                ", city='" + city + '\'' +
                '}';
    }

}
