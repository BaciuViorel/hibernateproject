package org.example.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;

@Entity
public class Person {
    @Id
    private String cnp;
    private String name;
    private Integer yearOfBirthday;
    @ManyToOne
    private Address address;

    public Person() {
    }

    public Person(String cnp, String name, Integer yearOfBirthday, Address address) {
        this.cnp = cnp;
        this.name = name;
        this.yearOfBirthday = yearOfBirthday;
        this.address = address;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getYearOfBirthday() {
        return yearOfBirthday;
    }

    public void setYearOfBirthday(Integer yearOfBirthday) {
        this.yearOfBirthday = yearOfBirthday;
    }

    @Override
    public String toString() {
        return "Person{" +
                "cnp='" + cnp + '\'' +
                ", name='" + name + '\'' +
                ", yearOfBirthday=" + yearOfBirthday +
                ", address=" + address +
                '}';
    }
}
