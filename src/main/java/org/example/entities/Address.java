package org.example.entities;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;

@Entity
public class Address {
    @EmbeddedId // pentru ca e vorba de mai mult coloane care vor forma cheia primara
    private AddressID id;

    private Integer squareMeters;

    @Enumerated(value = EnumType.STRING)
    private BuildingType buildingType;

    public Address() {
    }

    public Address(AddressID id, Integer squareMeters, BuildingType buildingType) {
        this.id = id;
        this.squareMeters = squareMeters;
        this.buildingType = buildingType;
    }

    public AddressID getId() {
        return id;
    }

    public void setId(AddressID id) {
        this.id = id;
    }

    public Integer getSquareMeters() {
        return squareMeters;
    }

    public void setSquareMeters(Integer squareMeters) {
        this.squareMeters = squareMeters;
    }

    public BuildingType getBuildingType() {
        return buildingType;
    }

    public void setBuildingType(BuildingType buildingType) {
        this.buildingType = buildingType;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", squareMeters=" + squareMeters +
                ", buildingType=" + buildingType +
                '}';
    }
}
