package org.example.dao;

import org.example.database.DataBaseConfig;
import org.example.entities.Address;
import org.example.entities.AddressID;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class AddressDao {

    public Address createAddress(Address address){
        Session session = DataBaseConfig.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.persist(address);

        transaction.commit();
        session.close();

        return address;
    }
    public Address readAddress(AddressID addressID){
        Session session = DataBaseConfig.getSessionFactory().openSession();
        // am salvat sesiunea  intr-un obiect
        Address a1 = session.find(Address.class, addressID);

        session.close();
        return a1;
    }
    public List<Address> readAllAdress(){
        Session session = DataBaseConfig.getSessionFactory().openSession();
        // pentru sesiunea mea o sa fac un query si voi lua rezultatul acestui query
        // acest query va fi un query pentru hibernate
        // cauta in entitate
        // Address este numele entitatii (al entitatii)
        List<Address> addresses = session.createQuery("select a from Address a",Address.class).getResultList();
        session.close();
        return addresses;
    }
    public Address updateAddress (Address address) {
        Session session = DataBaseConfig.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.merge(address);
        transaction.commit();
        session.close();
        return address;
    }
    public void removeAdress(Address address){
        Session session = DataBaseConfig.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.remove(address);

        transaction.commit();
        session.close();
    }
}
